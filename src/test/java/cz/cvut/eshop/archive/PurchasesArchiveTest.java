package cz.cvut.eshop.archive;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import cz.cvut.eshop.TestData;
import cz.cvut.eshop.shop.Order;

public class PurchasesArchiveTest extends AbstractArchiveTest {

	private PurchasesArchive emptyArchive;
	private PurchasesArchive notEmptyArchive;
	private Order order;

	@Override
	@Before
	public void setUp() {
		super.setUp();
		emptyArchive = new PurchasesArchive();
		notEmptyArchive = TestData.createPurchaseArchive(item.getID(), itemArchive);
		order = TestData.createOrder(item);
	}

	@Test
	public void testPrintItemPurchaseStatistics() throws IOException {
		PrintStream originalStdout = System.out;
		ByteArrayOutputStream buffer = new ByteArrayOutputStream();
		PrintStream newStdout = new PrintStream(new BufferedOutputStream(buffer), true);
		System.setOut(newStdout);
		emptyArchive.printItemPurchaseStatistics();
		Assert.assertTrue("Empty archive purchase statistic", buffer.size() > 0);
	}

	@Test(expected = NullPointerException.class)
	public void testGetHowManyTimesHasBeenItemSoldNull() {
		emptyArchive.getHowManyTimesHasBeenItemSold(null);
	}

	@Test
	public void testGetHowManyTimesHasBeenItemSoldEmpty() {
		int soldCount = emptyArchive.getHowManyTimesHasBeenItemSold(item);
		Assert.assertEquals("Not present item sold multiple times", 0, soldCount);
	}

	@Test
	public void testGetHowManyTimesHasBeenItemSold() {
		int soldCount = notEmptyArchive.getHowManyTimesHasBeenItemSold(item);
		Assert.assertEquals("Item sold incorrect number of times", itemArchive.getCountHowManyTimesHasBeenSold(), soldCount);
	}

	@Test(expected = NullPointerException.class)
	public void testPutOrderToPurchasesArchiveNull() {
		emptyArchive.putOrderToPurchasesArchive(null);
	}

	@Test
	public void testPutOrderToPurchasesArchiveEmpty() {
		emptyArchive.putOrderToPurchasesArchive(order);
		int soldCount = emptyArchive.getHowManyTimesHasBeenItemSold(item);
		Assert.assertEquals("Item sold multiple times in empty archive", 1, soldCount);
	}

	@Test
	public void testPutOrderToPurchasesArchive() {
		int previousSoldCount = notEmptyArchive.getHowManyTimesHasBeenItemSold(item);
		notEmptyArchive.putOrderToPurchasesArchive(order);
		int newSoldCount = notEmptyArchive.getHowManyTimesHasBeenItemSold(item);
		Assert.assertEquals("Item sold incorrect number of times", previousSoldCount + 1, newSoldCount);
	}
}